@extends('layouts.app')

@section('content')

@if($classified)

@php 
// $labels = [
//     0 => 'Z-Team',
//     1 => 'Diospero'        

// ];
// foreach ($classified->classifiedImages as $image) {

    
//     if ($image->count() > 1){
        
//         $thislabels = $image->labels;
//         // $a = array_values($thislabels);
        
//         $labels =$labels + $thislabels;


//         $uniquelabels = array_unique($labels);
//     } else {
//         $uniquelabels = $image->labels;
//     }
// }
@endphp


    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Annuncio #{{ $classified->id }} 
                          <a href="#acceptrefuse"> Go Down </a>
                    </div>
                    <div class="card-body">
                        <div class="row py-2">
                            <div class="col-2">
                                <h5 class="text-capitalize">{{__('ui.user')}}</h5>
                            </div>
                            <div class="col-6">
                                {{ $classified->user->name }} 
                                
                            </div>
                            <div class="col-4">
                                <span class="lead">
                                    Totale Inserimenti {{$classified->user->userclassifieds->count()}}
                                </span><br>
                                <span class="lead">
                                  Approvati : {{$classified->user->userclassifieds->where('is_accepted', true)->count()}}
                                </span><br>
                                <span class="lead">
                               Rifiutati : {{$classified->user->userclassifieds->where('is_accepted', 'null')->count()}}
                                </span><br>
                                <span class="lead"> Numero immagini {{$classified->classifiedImages->count()}}</span>
                                <div class="d-block">
                                  
                                        
                                   
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h5 class="text-capitalize">{{__('ui.email')}}</h5>
                            </div>
                            <div class="col-md-10">{{ $classified->user->email }}</div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-2">
                                <h5 class="text-capitalize">{{__('ui.title')}}</h5>
                            </div>
                            <div class="col-md-10">{{ $classified->title }}</div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-2">
                                <h5 class="text-capitalize">{{__('ui.description')}}</h5>
                            </div>
                            <div class="col-md-10">{{ $classified->description }}</div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-2">
                                <h5 class="text-capitalize">{{__('ui.price')}}</h5>
                            </div>
                            <div class="col-md-10">{{ $classified->price }}</div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-2">
                                <h5 class="text-capitalize">{{__('ui.place')}}</h5>
                            </div>
                            <div class="col-md-10">{{ $classified->location }}</div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-2">
                                <h5 class="text-capitalize">{{__('ui.images')}}</h5>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    @foreach ($classified->classifiedImages as $image)
                                    <div class="col-6">
                                        <img src="{{$image->geturl(400,300)}}" class="img-fluid m-3">
                                    </div>
                                    <div class="col-6">
                                        <div class="progress">
                                            <div class="progress-bar"  style="width:{{$image->adult}}%" role="progressbar" aria-valuenow="{{$image->adult}}" aria-valuemin="0" aria-valuemax="100"> Adult: </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar"   style="width:{{$image->spoof}}%"role="progressbar" aria-valuenow="{{$image->spoof}}" aria-valuemin="0" aria-valuemax="100"> Spoof: </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar"  style="width:{{$image->medical}}%" role="progressbar" aria-valuenow="{{$image->medical}}" aria-valuemin="0" aria-valuemax="100"> Medical: </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar"  style="width:{{$image->violence}}%" role="progressbar" aria-valuenow="{{$image->violence}}" aria-valuemin="0" aria-valuemax="100"> Violence: </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar"  style="width:{{$image->racy}}%" role="progressbar" aria-valuenow="{{$image->racy}}" aria-valuemin="0" aria-valuemax="100"> Racy: </div>
                                        </div>

                                        @if($image->labels)
                                            <ul>
                                            @foreach($image->labels as $label)
                                                <li>{{$label}}</li>
                                            @endforeach
                                            </ul>
                                        @endif

                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="row justify-content-center py-5" id="acceptrefuse">
            <div class="col-4">
                <form action="{{ route('revisor.accept', $classified->id) }}" method="POST">
                    @csrf
                    <button class="btn btn-success btn-lg" type="submit"> ACCETTA </button>
                </form>
            </div>
            <div class="col-4 text-right">
                <form action="{{ route('revisor.reject', $classified->id) }}" method="POST">
                    @csrf
                    <button class="btn btn-danger btn-lg" type="submit"> RIFIUTA </button>
                </form>

            </div>


        </div>



    </div>

@else 

<div class="container py-5">
    <div class="row">
        <div class="col-12 text-center">
        <h3>Non hai annunci da revisionare :-)</h3>
        </div>
    </div>
</div>
@endif
@endsection
