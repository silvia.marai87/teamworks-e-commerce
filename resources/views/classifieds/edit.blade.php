@extends('layouts.app')

@section('content')
<div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1>Modifica annuncio</h1>
            </div>
        </div>
    </div>
<div class="container py-5">
  <div class="row">
      <div class="col-12 bg-white shadow py-3">
<form id="classified" action="{{route('classifieds.update', compact('classified'))}}" method="POST">
@method('PUT')
@csrf
    
  <div class="form-group">
    <label for="title">Name</label>
    <input value="{{$classified->title}}" type="text" name="title" class="form-control" id="title" aria-describedby="title">
    @error('title')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
    </div>
  <div class="form-group">
    <label for="classifiedDescription">Description</label>
    <input value="{{$classified->description}}" type="text" name="description" class="form-control" id="classifiedDescription" aria-describedby="emailHelp">
    @error('description')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>
 
  <div class="form-group">
    <select name="category_id" id="category_id" value="{{ old('category_id') }}"     class="form-control @error('category_id') is-invalid @enderror"            aria-describedby="category_id">
      <option value="" selected>SCEGLI UNA CATEGORIA</option>
      @foreach($categories as $category)
      <option value="{{ $category->id }}"><h3>{{ $category->title }}</h3></option>
      @endforeach
  </select> 
    @error('category_id')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
 </div>

 
  <div class="form-group">
    <label for="price">Price</label>
    <input value="{{$classified->price}}"  type="number" name="price" class="form-control" id="price" aria-describedby="price">
    @error('price')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
 </div>
 
 <div class="form-group">
    <label for="location">Location</label>
    <input value="{{$classified->location}}"  type="text" name="location" class="form-control" id="location" aria-describedby="location">
    @error('location')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
 </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
</div>
@endsection