@extends('layouts.app')

@section('content')
<div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1>Inserisci annuncio</h1>
            </div>
        </div>
    </div>
<div class="container py-5">
    <div class="row">
        <div class="col-12 bg-white shadow py-3">
         
            <form id="classified" action="{{ route('classifieds.store') }}" method="post">
                @csrf
                <input type="hidden" value="{{old('uniqueSecret', $uniqueSecret)}}" name="uniqueSecret" id="uniqueSecret">
                <div class="form-group">
                    <label for="title">{{__('classified.name')}}</label>
                    <input type="text" name="title" value="{{ old('title') }}"
                        class="form-control @error('title') is-invalid @enderror" id="title" aria-describedby="title">
                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="description">{{__('classified.description')}}</label>
                    <input type="text" name="description" value="{{ old('description') }}"
                        class="form-control @error('description') is-invalid @enderror" id="description"
                        aria-describedby="description">
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="image" class="col-md-12 col-form-label text-right">{{__('classified.images')}}</label>
                    <div class="col-12">

                        <div class="dropzone" id="drophere"></div>

                        @error('images')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>



                <div class="form-group">
                    <label for="category_id">{{__('classified.category')}} <label>
                            <select name="category_id" id="category_id"
                                value="{{ old('category_id') }}"
                                class="form-control @error('category_id') is-invalid @enderror"
                                aria-describedby="category_id">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            </select>

                            @error('category_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                </div>

                <div class="form-group">
                    <label for="price">{{__('classified.price')}}</label>
                    <input type="number" name="price" value="{{ old('price') }}"
                        class="form-control @error('price') is-invalid @enderror" id="price" aria-describedby="price">
                    @error('price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="location">{{__('classified.location')}}</label>
                    <input type="text" name="location" value="{{ old('location') }}"
                        class="form-control @error('location') is-invalid @enderror" id="location"
                        aria-describedby="location">
                    @error('location')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <button type="submit" class="btn btn-dark">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
