@extends('layouts.app')
@section('content')


<div class="container">
    <div class="row">
      <div class="col-12">
       <form id="newImage" action="{{route('classified.image.store', compact('classified'))}}" method="POST" enctype="multipart/form-data">
          @csrf
         
           
          
               
                  <div class="custom-file">
                    <label class="custom-file-label" for="file">Nuova IMMAGINE</label>
                    <input type="file" class="custom-file-input" name="file" id="file">
                  </div>
                </div>
              
          <button type="submit" class="btn btn-primary">Submit</button>
        </form> 
      </div>
    </div>
  </div> 
@endsection
