@extends('layouts.app')
@section('content')

<div class="container bg-white border-0 shadow my-5 py-2">
    <div class="row">
        <!-- Carosello -->
        <div class="col-6 p-3">
            <div class="row justify-content-center">
                
                <div class="col-10 slide-big">
                    @foreach ($classified->classifiedImages as $image)
                    <div>
                        <img src="{{$image->geturl(400,300)}}" class="img-fluid m-auto">
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-10 slide-controll">
                    @foreach ($classified->classifiedImages as $image)
                    <div>
                        <img src="{{$image->geturl(400,300)}}" class="img-fluid m-auto">
                    </div>
                    @endforeach
                </div>
            </div> 
        </div>
        
        <!-- Body -->
        <div class="col-6">
            <div class="card-body display-flex">
                <div class="bottomLine mb-2">
                    <h1>{{$classified->title}}</h1>
                    <h5 class="text-capitalize">{{__('ui.category')}} <a href="{{route('classifieds.categoryFiltered', $classified->category_id)}}"><strong>{{$classified->category->title}}</strong></a></h5>
                    <a href="{{route('classifieds.categoryFiltered', $classified->category_id)}}">
                    <img src="{{Storage::url($classified->category->categoryHeader)}}"  class="w-25 img-fluid" alt="">
                    </a>
                    @php 
                    $user = $classified->user;
                    
                    @endphp
                    <p class="card-text mt-2"> Venduto da:</p>
                    <div class="d-block w-100">
                        
                        <img src="{{ Storage::url($classified->user->avatar) }}" alt="" class=" mb-3 img-fluid rounded-circle w-25">
                        <a href="{{route('user.article', compact('user'))}}">{{ $classified->user->name }} con un totale di {{App\Classified::userAppovedAdsCount($classified->user)}} articoli</a>
                        
                        
                    </div>
                    
                </div>
                <span class="text-capitalize fw-700">{{__('ui.place')}} :</span>
                <span class="card-text">{{$classified->location}}</span>
                <br>
                <p class="text-capitalize h4 fw-700 mt-3">
                    {{__('ui.article-desc')}}
                </p>
                <p class="card-text">{{$classified->description}}</p>
                <h3 class="card-text prezzo"><span>€ <strong>{{$classified->price}}</strong> </span></h3>
                <button class="btn btn-warning rounded-5 center ml-auto">Compra ora</button>
                <p class="card-text">{{$classified->status}}</p>
                
            </div>
        </div>
    </div>
</div>

<div class="container my-3">
    <h2 class="text-center display-3"> Altri Articoli in questa categoria </h2>
    <div class="row justify-content-center mt-5">
        <div class="col-12">
            
            @include('categories.includes._otherarticle')
        </div>
    </div>
</div>
<div class="container my-3">
    <h2 class="text-center display-3"> Ultimi Articoli Inseriti </h2>
    <div class="row justify-content-center mt-5">
        <div class="col-12">
            
            <div id="lastArticle">
                
                
                @foreach (\App\Classified::last()->take(5) as $classified)
                @php
                $image = $classified->classifiedImages->pop();  
                
                if ($classified->is_accepted) {
                    $is_acceptedClass = "bg-success";
                } elseif ($classified->is_accepted != '0' && $classified->is_accepted != '1') {
                    
                    $is_acceptedClass = "bg-success";
                } else {
                    $is_acceptedClass = "bg-danger";
                }
                
                
                @endphp
                <div class=" px-4">
                    <div class="card">
                        <div class="card-body">
                            
                            <img src="{{$image->geturl(400,300)}}" class="card-img img-fluid" alt="{{ $classified->title }}">
                            
                            <h5 class="card-title mt-3">{{ $classified->title }}</h5>
                            <p class="card-text">Descrizione {{ $classified->description }}</p>
                            <p class="card-text">Località: {{ $classified->location }}</p>
                            <p class="card-text">Prezzo: {{ $classified->price }}</p>
                            <p class="card-text"> Venduto da: <a href="{{route('classifieds.categoryFiltered', $classified->category_id)}}">{{ $classified->category->title}}</a></p>
                            <p class="card-text"> Venduto da: <a href="{{route('user.article', $classified->user->id)}}">{{ $classified->user->name}}</a></p>
                            <a href="{{route('classifieds.show', compact('classified'))}}" class="btn btn-dark">Vai all'annuncio</a>
                        </div>
                    </div> 
                </div>
                @endforeach 
            </div>
        </div>
    </div>
</div>
<div class="container my-3">
    <h2 class="text-center display-3"> Altri Articoli di questo utente </h2>
    <div class="row justify-content-center mt-5">
        <div class="col-12">
            
            @include('users.includes._otherarticle')
        </div>
    </div>
</div>
@endsection


@push('singlepagescript')
<script>
    $('.slide-big').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slide-controll'
    });
    $('.slide-controll').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slide-big',
        dots: true,
        centerMode: true,
        focusOnSelect: true
    });
    $('#otherArticleUser').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        centerMode: true,
    });
    $('#otherArticleCategory').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        centerMode: true,
    });
    $('#lastArticle').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        centerMode: true,
    });
</script>
@endpush
