@extends('layouts/app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-12">
      <form id="user" action="{{route('user.update.profile', compact('user'))}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="categoryName"> user Name </label>
          <input value="{{$user->name}}" type="text" name="title" class="form-control" id="name" aria-describedby="emailHelp">
          <input value="{{$user->id}}" type="hidden" name="userId">
        </div>
         
        <div class="row">
          <div class="col-12">
            <div class="row">
              <div class="col-6">
                <div class="w-50 m-auto">
                  <img src="{{ Storage::url($user->avatar) }}" alt="" class="img-fluid">
                </div>
              </div>
              <div class="col-6">
                <div class="custom-file">
                  <label class="custom-file-label" for="userThumb">Avatar</label>
                  <input type="file" class="custom-file-input" name="avatar" id="avatar">
                </div>
              </div>
            </div> 
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>

@endsection