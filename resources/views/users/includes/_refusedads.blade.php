<h2> Ecco i tuoi articoli Rifiutati </h2>
<div class="row justify-content-center">
    @foreach (\App\Classified::userRefusedAds($user) as $classified)
    @php
    // dd($classified->is_accepted);
    $image = $classified->classifiedImages->pop();  
    if ($classified->is_accepted != '0' && $classified->is_accepted != '1') {
        $is_acceptedClass = "bg-warning";
    } elseif ($classified->is_accepted === '1') {
        
        $is_acceptedClass = "bg-success";
    } else {
        $is_acceptedClass = "bg-danger";
    }
    
    @endphp
    <div class="col-12 col-md-4 py-2">
        <div class="card border-0 shadow {{$is_acceptedClass}}">
            <div class="card-body">
                @if ($image === null)
                <img src="https://picsum.photos/400/300" alt="" class="img-fluid">
                @else
                <img src="{{$image->geturl(400,300)}}" class="card-img img-fluid" alt="{{ $classified->title }}">
                @endif                
                
                <h5 class="card-title mt-3">{{ $classified->title }}</h5>
                <p class="card-text">Descrizione {{ $classified->description }}</p>
                <p class="card-text">Località: {{ $classified->location }}</p>
                <p class="card-text">Prezzo: {{ $classified->price }}</p>
                <p class="card-text"> Venduto da: {{ $classified->user->name }}</p>
                <a href="{{route('classifieds.show', compact('classified'))}}" class="btn btn-dark">Vai all'annuncio</a>
            </div>
        </div>
    </div>
    @endforeach 
</div>