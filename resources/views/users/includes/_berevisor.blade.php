                             
                                @if ($user->is_revisor)
                                <div class="card bg-main">
                                    <div class="card-body">
                                    <h3 class="card-title mt-3">Invita i tuoi amici a diventare revisore</h6>
                                    <h3 class="card-text">Guadagna anche dalle loro revisioni</h6>
                                    <a class="btn btn-primary" href="{{route('revisor.landing')}}" target="_blank"> Scopri Come </a>
                                    </div>
                                </div>
                                @elseif ($revisorList === [])
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <a href="{{route('revisor.request', compact('user'))}}" class="btn btn-dark">Diventa revisore</a>
                                        </div>
                                    </div>
                                </div>
                                @else 
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 bg-success">
                                            @php 
                                            if (isset($revisorRequest))
                                            
                                            {
                                                $date = new Datetime($revisorRequest['created_at']);
                                                $date = $date->format('d M Y');
                                            }
                                            
                                            @endphp
                                            STIAMO ELABORANDO LA SUA RICHIESTA del<span class="h3 d-block">{{$date}}</span>
                                        </div>
                                    </div>
                                </div>
                                
                                @endif  