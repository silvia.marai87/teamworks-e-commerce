<div id="otherArticleUser">
                
                
                @foreach (\App\Classified::userAppovedAds($user)->take(5) as $classified)
                @php
                $image = $classified->classifiedImages->pop();  
                
                if ($classified->is_accepted) {
                    $is_acceptedClass = "bg-success";
                } elseif ($classified->is_accepted != '0' && $classified->is_accepted != '1') {
                    
                    $is_acceptedClass = "bg-success";
                } else {
                    $is_acceptedClass = "bg-danger";
                }
                
                
                @endphp
                <div class=" px-4">
                    <div class="card">
                        <div class="card-body">
                            @if ($image === null)
                            <img src="https://picsum.photos/400/300" alt="" class="img-fluid">
                            @else
                            <img src="{{$image->geturl(400,300)}}" class="card-img img-fluid" alt="{{ $classified->title }}">
                            @endif
                            
                            
                            
                            <h5 class="card-title mt-3">{{ $classified->title }}</h5>
                            <p class="card-text">Descrizione {{ $classified->description }}</p>
                            <p class="card-text">Località: {{ $classified->location }}</p>
                            <p class="card-text">Prezzo: {{ $classified->price }}</p>
                            <p class="card-text"> Nella Categoria : <a href="{{route('classifieds.categoryFiltered', $classified->category_id)}}">{{ $classified->category->title}}</a></p>
                            <p class="card-text"> Venduto da: <a href="{{route('user.article', $classified->user->id)}}">{{ $classified->user->name}}</a></p>
                            <a href="{{route('classifieds.show', compact('classified'))}}" class="btn btn-dark">Vai all'annuncio</a>
                        </div>
                    </div> 
                </div>
                @endforeach 
            </div>