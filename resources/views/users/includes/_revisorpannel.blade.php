@if ($user->is_revisor)
<h2>Pannello Revisore</h2>
@php 
$count = \App\Classified::toBeRevisedCount();
$revisorRequest = \App\revisorRequest::toBeCheckCount();
@endphp 
@if ( $count > 0 )
<h4 class="text-center bg-danger py-4 white">Hai {{\App\Classified::toBeRevisedCount()}} annunci da revisionare</h4>
@else 
<h4 class="text-center bg-success py-4 white"> Non hai annunci da revisionare </h4>
@endif
<div class="container mb-5 mt-3">
    <div class="row justify-content-center">
        <div class="col-4 text-center">
            <a href="{{route('revisor.home')}}" class="btn btn-warning "> Vai alla pagina di revisione </a>
        </div>
    </div>
</div>
@endif 