@extends('layouts.app')
@section('content')

@php
$revisorList = \App\RevisorRequest::where('user_email', $user->email)->get();
$revisorList =   json_decode(json_encode($revisorList), true);

// dd($revisorList);
if ($revisorList !== []){
    
    $revisorRequest = $revisorList[0];

}
$r = route('user.profile');

@endphp
}

        @include('users.includes._profileheader')
            <div class="container bg-white shadow">
                <div class="row p-3">
                    <div class="col-5">
                        @include('users.includes._useravatar')
                        @include('users.includes._userinfo')
                        @if (Auth::user() && Auth::user() != $user)
                            
                        @else
                        <a href="{{route('user.edit.profile')}}" class="btn btn-danger p-3"> Edita Profilo </a>
                        @endif
            
                        @if (Auth::user() && Auth::user()->is_admin)
                        <a href="{{route('admin.user.edit.profile', compact('user'))}}" class="btn btn-danger p-3"> God Mode</a>
                        @endif
                        @include('users.includes._revisorpannel')
                    </div>
                    <div class="col-7">

                        <p class="lead">Qui di solito ci va qualcosa ma non ci sono le funzionalità come carrello, vecchi ordini, indirizzi</p>
                        @include('users.includes._berevisor')               
                        <p class="lead">la parte nel mezzo cambia a seconda del ruolo che ricopri. Tranne se sei Giovanni. Giovanni Paga da Bere a Tutti.</p>         
                    </div>
                </div>
            </div>
            
            
            
<div class="container bg-white shadow">
    @include('users.includes._toberevisedads')
    
    
    @include('users.includes._refusedads')
    @include('users.includes._approvedads')
    
</div>            
   

@endsection