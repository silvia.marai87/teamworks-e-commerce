@extends('layouts.app')
@section('content')

<div class="container py-5">
    <div class="row">
        <div class="col-12">
            
            <div class="row">
                <div class="col-4">

                    <img src="{{Storage::url($user->avatar)}}" class="img-fluid" alt="">
                </div>
                <div class="col-8">
                    <h2>Annunci dell'utente {{$user->name}} </h2>
                    <h3>Totale annunci di questo utente {{\App\Classified::userAppovedAdsCount($user)}}
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="container">
    <div class="row">
      <div class="col-12">
        @foreach($classifieds as $classified)

        @php
            $image = $classified->classifiedImages->pop();  
        @endphp 


            <div class="card mb-3 border-0 shadow">
                <div class="row no-gutters">
                    <div class="col-md-3 p-3">
                        <a href="{{route('classifieds.show', compact('classified'))}}">
                            @if ($image)
                            <img src="{{$image->geturl(400,300)}}" class="card-img img-fluid" alt="{{ $classified->title }}">
                            @else   

                            @endif
                        
                        </a>
                    </div>
                    <div class="col-md-9">
                        <div class="card-body">
                             
                            <h5 class="card-title mt-3">{{ $classified->title }}</h5>
                            <p class="card-text">Descrizione {{ $classified->description }}</p>
                            <p class="card-text">Località: {{ $classified->location }}</p>
                            <p class="card-text">Prezzo: {{ $classified->price }}</p>
                            <p class="card-text"> Nella Categoria : <a href="{{route('classifieds.categoryFiltered', $classified->category_id)}}">{{ $classified->category->title}}</a></p>
                            <p class="card-text"> Venduto da: <a href="{{route('user.article', $classified->user->id)}}">{{ $classified->user->name}}</a></p>
                            <a href="{{route('classifieds.show', compact('classified'))}}" class="btn btn-dark">Vai all'annuncio</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
      </div>
    </div>
</div> 

@endsection