@extends('layouts.app')
@section('content')

<header>
    
    <div class="col-12 text-center">
        
    </div>
    
    <!-- Carosello header -->
    <div id="carouselHeader" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselHeader" data-slide-to="0" class="active"></li>
            <li data-target="#carouselHeader" data-slide-to="1"></li>
            <li data-target="#carouselHeader" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One -->
            <div class="carousel-item active" style="background-image: url('https://www.muminjob.it/wp-content/uploads/2019/01/Making-Money-Online-1.jpg')">
                <div class="carousel-caption d-none d-md-block">
                    <h3 class="display-4">Diventa Revisore di Annunci</h3>
                    <p class="lead">Guadagna restando a casa. <a href="{{route('revisor.landing')}}" target="_blank" class="btn bg-main display-4">Scopri come</a></p>
                </div>
                <div class="container py-3 bg-main mt-3">
                    <div class="row">
                        <div class="col-12">
                            <h1 class="h3 text-center">{{__('ui.welcome')}} Z-Market
                            <span class="h4">L'ultimo stato di annunci di cui avrai mai bisogno!</span>   </h1>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slide Two -->
            <div class="carousel-item" style="background-image: url('https://picsum.photos/1920/1081')">
                <div class="carousel-caption d-none d-md-block">
                    <h3 class="display-4">Second Slide</h3>
                    <p class="lead">This is a description for the second slide.</p>
                </div>
                <div class="container py-3 bg-main mt-3">
                    <div class="row">
                        <div class="col-12">
                            <h1 class="h3 text-center">{{__('ui.welcome')}} Z-Market
                            <span class="h4">L'ultimo sito di annunci di cui avrai mai bisogno!</span>   </h1>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slide Three -->
            <div class="carousel-item" style="background-image: url('https://picsum.photos/1920/1082')">
                <div class="carousel-caption d-none d-md-block">
                    <h3 class="display-4">Third Slide</h3>
                    <p class="lead">This is a description for the third slide.</p>
                </div>
                <div class="container py-3 bg-main mt-3">
                    <div class="row">
                        <div class="col-12">
                            <h1 class="h3 text-center">{{__('ui.welcome')}} Z-Market
                            <span class="h4">L'ultimo sito di annunci di cui avrai mai bisogno!</span>   </h1>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselHeader" role="button" data-slide="prev">
            <span class="fa-2x" aria-hidden="true"><i class="fas fa-chevron-circle-left"></i></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselHeader" role="button" data-slide="next">
            <span class="fa-2x" aria-hidden="true"><i class="fas fa-chevron-circle-right"></i></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>

@include('includes._categoriesHome')

@include('includes._lastArticle')
    
    
    
    <div class="container py-5">
        <div class="row">
            {{-- {{ dd($user->userclassifieds) }} --}}
            @if(Auth::check())
            <div class="col-12">
                <h2 class="mb-3 text-center">Ciao {{ $user->name }}, ecco i tuoi annunci!</h2>
            </div>
            @foreach( \App\Classified::userAppovedAds($user)->sortByDesc('id') as $classified)
            <div class="col-12 col-md-4 py-2">
                <div class="card border-0 shadow">
                    <div class="card-body">
                        @php
                        $image = $classified->classifiedImages->pop();  
                        @endphp 
                        @if($image)    
                        <a href="{{route('classifieds.show', compact('classified'))}}">
                            <img src="{{$image->geturl(400,300)}}" class="img-card-top img-fluid">
                        </a>
                        @else
                        <a href="{{route('classifieds.show', compact('classified'))}}">
                            <img src="https://via.placeholder.com/400x300" class="img-card-top img-fluid">
                        </a>
                        @endif
                        <h5 class="card-title mt-3">{{ $classified->title }}</h5>
                        <p class="card-text">Descrizione {{ $classified->description }}</p>
                        <p class="card-text">Località: {{ $classified->location }}</p>
                        <p class="card-text">Prezzo: {{ $classified->price }}</p>
                        <p class="card-text"> Venduto da: {{ $classified->user->name }}</p>
                        <a href="{{route('classifieds.show', compact('classified'))}}" class="btn btn-dark">Vai all'annuncio</a>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <h3>Non sei loggato!</h3>
            @endif
        </div>
    </div>
    
    @endsection
    
    @push('singlepagescript')
    <script>
        $('#slidehome').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
            ]
        });
        
    </script>
    @endpush
    