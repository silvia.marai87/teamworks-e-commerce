@extends('layouts/app')

@section('content')
@php 
$i = 0;
@endphp
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1>Categorie</h1>
                <a href="{{route('categories.create')}}" class="btn btn-dark my-3">Crea Nuova</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 shadow bg-white mb-5">
                <table class="table table-over mt-3">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">
                                ID 
                            </th>
                            <th scope="col">
                                Categoria
                            </th>
                            <th scope="col">
                                Shortcode
                            </th>
                            <th scope="col">
                                Thumb
                            </th>
                            <th scope="col">
                                Header
                            </th>
                            <th scope="col">
                                Articoli
                            </th>
                            <th scope="col">
                                Approved
                            </th>
                            <th scope="col">
                                Pending
                            </th>
                            <th scope="col">
                                Refused
                            </th>
                            <th scope="col">
                                Modifica
                            </th>
                            <th scope="col">
                                Elimina
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $category)
                   
                    <tr>
                    <td class="text-center h3" style="vertical-align: middle">{{$category->id}}</td>
                    <td class="text-center h3" style="vertical-align: middle">{{$category->title}}</td>
                    <td class="text-center h3" style="vertical-align: middle">{{$category->shortTitle}}</td>
                    <td>
                        <a href="" data-toggle="modal" data-target="#thumb-{{$i}}">
                            <img src="{{ Storage::url($category->categoryThumb) }}" alt="" class="img-fluid w-50">
                        </a>
                        <div class="modal fade" id="thumb-{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <img src="{{ Storage::url($category->categoryThumb) }}" alt="" class="img-fluid">
                              </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <a href="" data-toggle="modal" data-target="#header-{{$i}}">
                        <img src="{{ Storage::url($category->categoryHeader) }}" alt="" class="img-fluid w-50">
                        </a>
                        <div class="modal fade" id="header-{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <img src="{{ Storage::url($category->categoryHeader) }}" alt="" class="img-fluid">
                              </div>
                            </div>
                        </div>
                    </td>
                    <td class="text-center h3" style="vertical-align: middle">
                        {{App\Classified::where('category_id', $category->id)->count()}}
                    </td>
                    <td class="text-center h3" style="vertical-align: middle">
                        {{App\Classified::where('is_accepted', true)->where('category_id', $category->id)->count()}}
                    </td>
                    <td class="text-center h3" style="vertical-align: middle">
                        {{App\Classified::where('is_accepted', null)->where('category_id', $category->id)->count()}}
                    </td>
                    <td class="text-center h3" style="vertical-align: middle">
                        {{App\Classified::where('is_accepted', false)->where('category_id', $category->id)->count()}}
                    </td>
                    <td><a href="{{route('categories.edit', compact('category'))}}" class="btn btn-primary"> Modifica </a></td>
                    
                    
                    <td> <!-- Funzione delete all'interno della modale -->    
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modale_{{$category->id}}">Elimina</button>      
                    <div class="modal fade" id="modale_{{$category->id}}" tabindex="-1" aria-labelledby="label_modale" aria-hidden="true"> 
                    <div class="modal-dialog modal-dialog-centered"> 
                    <div class="modal-content">                            
                    <div class="modal-header">                                 
                    <h5 class="modal-title" id="label_modale">Elimina categoria</h5>                                 <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close"><span aria-hidden="true">&times;</span></button>     
                    </div>
                    <div class="modal-body">Sei sicuro di voler eliminare la categoria? L'operazione non è reversibile!</div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                    <form action="{{route('categories.delete', compact('category'))}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Elimina categoria<button>

                    </form>
                    </div>
                    </div>
                    </div>
                    </div>
                    </td>
                    </tr>
                    @php
                    $i++;    
                    @endphp
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection