@extends('layouts.app')

@section('content')
<div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1>Inserisci categoria</h1>
            </div>
        </div>
  </div>

<div class="container py-5">
  <div class="row">
      <div class="col-12 bg-white shadow py-3">
        <form id="category" action="{{route('categories.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="categoryName">Category Name</label>
          <input type="text" name="title" class="form-control" id="categoryName" aria-describedby="emailHelp">
        </div>
        <div class="form-group">
          <label for="shortTitle">Shortcode</label>
          <input type="text" name="shortTitle" class="form-control" id="shortTitle">
        </div>
        <div class="custom-file">
          <label class="custom-file-label" for="categoryThumb">Thumb</label>
          <input type="file" class="custom-file-input" name="categoryThumb" id="categoryThumb">
        </div>
        <div class="custom-file">
          <label class="custom-file-label" for="categoryHeader">Header</label>
          <input type="file" class="custom-file-input" name="categoryHeader" id="categoryHeader">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
      </div>
</div>
@endsection