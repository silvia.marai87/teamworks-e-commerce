@extends('layouts/app')

@section('content')
<div class="container pb-5">
    <div class="row">
        <div class="col-12">
            <h1>Annunci</h1>
            <a href="{{route('classifieds.create')}}" class="btn btn-dark mt-3">Crea Nuovo</a>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 shadow bg-white mb-5">
            <table class="table table-over mt-3">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">
                            ID
                        </th>
                        <th scope="col">
                            Titolo annuncio
                        </th>
                        <th scope="col">
                            Descrizione
                        </th>
                        <th scope="col">
                            Categoria
                        </th>
                        <th scope="col">
                            Immagini
                        </th>
                        <th scope="col">
                            Prezzo
                        </th>
                        <th scope="col">
                            Luogo
                        </th>
                        <th scope="col">
                            Status
                        </th>
                        <th scope="col">
                            Visualizza
                        </th>
                        <th scope="col">
                            Modifica
                        </th>
                        <th scope="col">
                            Elimina
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($classifieds as $classified)
                    <tr>
                        <td>{{$classified->id}}</td>
                        <td>{{$classified->title}}</td>
                        <td>{{$classified->description}}</td>
                        <td>{{$classified->category->title}}</td>
                        <td class="text-center h3" style="vertical-align: middle">
                            <a href="{{route('single.classified.images', compact('classified'))}}">
                                {{$classified->classifiedImages->count()}}&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-images"></i>
                            </a>
                        </td>
                        <td>{{$classified->price}}</td>
                        <td>{{$classified->location}}</td>
                        @if ($classified->is_accepted === 1)
                        <td><div class="bg-success m-1 p-3"></div></td>
                        @elseif ($classified->is_accepted === 0)
                        <td>
                            <div class="bg-danger m-1 p-3"></div>
                        </td>
                        @else 
                        <td>
                            <div class="bg-warning m-1 p-3"></div>
                        </td>
                        @endif
                        
                        <td><a href="{{route('classifieds.show', compact('classified'))}}" class="btn btn-dark"> Dettaglio </a></td>
                        <td><a href="{{route('classifieds.edit', compact('classified'))}}" class="btn btn-success"> Modifica </a></td>
                        
                        
                        <td> <!-- Funzione delete all'interno della modale -->    
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modale_{{$classified->id}}">Elimina</button>      
                            <div class="modal fade" id="modale_{{$classified->id}}" tabindex="-1" aria-labelledby="label_modale" aria-hidden="true"> 
                                <div class="modal-dialog modal-dialog-centered"> 
                                    <div class="modal-content">                            
                                        <div class="modal-header">                                 
                                            <h5 class="modal-title" id="label_modale">Elimina annuncio</h5>                                 <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close"><span aria-hidden="true">&times;</span></button>     
                                        </div>
                                        <div class="modal-body">Sei sicuro di voler eliminare l'annuncio? L'operazione non è reversibile!</div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                            <form action="{{route('classifieds.delete', compact('classified'))}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Elimina annuncio</button>
                                                    
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection