<div class="container-fluid  py-5 bg-main">

    <div class="row justify-content-center">
        <div class="col-12 mt-2 mb-3">
            <h2 class="text-center">Categorie</h2>
        </div>
        <div class="col-12 mb-5">
            <div class="row">
                @foreach($categories as $thiscategory)
                @php
                $category = $thiscategory->id
                @endphp
                <div class="col-2 justify-content-center text-center">
                    <div class="">
                        <div class="card-body"> 
                                <a href="{{ route('classifieds.categoryFiltered', compact('category')) }}">
                                    <img src="{{ Storage::url($thiscategory->categoryThumb) }}" alt="{{ $thiscategory->title }}" class="img-fluid rounded-circle rounded-circle bg-white w-50 p-3">
                                </a>
                            <p class="card-text mt-3"><a href="{{ route('classifieds.categoryFiltered', compact('category')) }}"
                                class="h2 white">{{ $thiscategory->title }}</a></p>
                        </div>
                        
                    </div>
                    
                </div>
                @endforeach
            </div>
        </div>
    </div>
    
</div>