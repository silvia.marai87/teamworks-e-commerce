
    <footer class="container-fluid bg-dark text-light py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <img src="/images/logo.png" class="img-fluid w-75" id="footerlogo" alt="Z-MARKERT" />
                        <h5 class="pt-5"><b>Z-Team</b> S.p.A.</h5>
                        <p>Via Le Mani dal Naso, 12<br>00187 Roma</p>
                        <p>Partita IVA: IT19382004822</p>
                        <p>REA: 1983882</p>
                        
                    </div>
                    <div class="col-md-3 mt-4 mt-md-1">
                        <h4 class="pb-3">Link <b>Utili</b></h4>
                        <ul class="list-unstyled">
                            <li class="mt-2"><a class="white" href="{{ route('user.profile') }}">Profilo</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('register') }}">Registrazione</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('login') }}">Login</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('classifieds.create') }}">Creazione annuncio</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('revisor.home') }}">Home revisore</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 mt-4 mt-md-1">
                        <h4 class="pb-3">Menu <b>di Servizio</b></h4>
                        <ul class="list-unstyled">
                        @if (Auth::user() && Auth::user()->is_admin)
                            <li class="mt-2"><a class="white" href="{{ route('classifieds.index') }}">Elenco annunci</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('classifieds.create') }}">Nuovo annuncio</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('categories.index') }}">Elenco categorie</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('categories.create') }}">Nuova categoria</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('admin.users.list') }}">User List</a></li>
                            <li class="mt-2"><a class="white" href="{{ route('admin.revisor.userRequestList') }}">REvisor Request</a></li>
                        @endif
                            <li> menu per admin</li>
                        </ul>
                    </div>
                    <div class="col-md-3 mt-4 mt-md-1">
                        <h4>Seguici sui <b>Social</b></h4>
                        <i class="fab fa-facebook-square d-inline my-3 mr-2 fa-2x"></i>
                        <i class="fab fa-instagram-square d-inline my-3 mr-2 fa-2x"></i>
                        <i class="fab fa-youtube-square d-inline my-3 mr-2 fa-2x"></i>
                        
                    </div>
                </div>
            </div>
        </footer>

        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    @stack('singlepagescript')