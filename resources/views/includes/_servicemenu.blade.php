@if(Auth::check())
<div class="dropdown m-2">
    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        Menu Servizio Crud
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        
            <a class="dropdown-item bg-white" href="{{ route('categories.index') }}">Categorie</a>
            <a class="dropdown-item bg-white" href="{{ route('classifieds.index') }}">Annunci</a>
        
    </div>
</div>
@endif