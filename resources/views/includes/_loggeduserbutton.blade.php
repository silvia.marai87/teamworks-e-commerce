<li class="nav-item dropdown px-2">
    <div class="d-flex h-100">
    <a id="navbarDropdown" class="nav-link dropdown-toggle btn btn-login mt-1 align-self-center" href="#" role="button"
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
    <span class="h6 text-white">
        @if (Auth::user()&& Auth::user()->avatar)
               <img src="{{ Storage::url(Auth::user()->avatar) }}" alt="" class="img-fluid rounded-circle" style="width:30px">
        @else 
        <i class="fas fa-user text-secondary-zteam px-2"></i> 
        @endif
    </span>
    @if(Auth::user() && Auth::user()->is_revisor && \App\Classified::toBeRevisedCount() > 0  ) 
    <span class="badge px-2 py-1 badge-danger">
        {{ \App\Classified::toBeRevisedCount() }}
    </span>
    @endif
    @if (Auth::user() && Auth::user()->is_admin && \App\revisorRequest::toBeCheckCount() > 0 ) 
    <span class="badge px-2 py-1 badge-danger">
        {{ \App\revisorRequest::toBeCheckCount() }}
    </span>
    @endif
</a>

<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
    @if (Auth::user() && Auth::user()->is_admin)
    <a class="dropdown-item" href="{{ route('user.profile') }}"><span class="h5 mr-2"><i class="fas fa-crown mt-2 text-secondary-zteam bg-main "></i></span> {{ Auth::user()->name }} </a>
        
    @elseif (Auth::user() && Auth::user()->is_revisor)
        
        <a class="dropdown-item" href="{{ route('user.profile') }}"><span class="h5 mr-2"><i class="fas fa-user-secret"></i></span> {{ Auth::user()->name }} </a>
    @else
       
        <a class="dropdown-item" href="{{ route('user.profile') }}"> <span class="h5 mr-2"><i class="fas fa-user mr-2 h5"></i></span> {{ Auth::user()->name }} </a>
    @endif
    <hr>
    @if (Auth::user()->is_revisor && \App\Classified::toBeRevisedCount() >0)
    <a class="dropdown-item" href="{{ route('revisor.home') }}">
        <span class="h4 mr-2">
            <i class="fas fa-ad text-danger"></i>
        </span>
        <span class="mr-2"> Revisiona Articoli</span>
        <span class="float-right">
            <span class="badge px-2 py-1 badge-danger">
                {{ \App\Classified::toBeRevisedCount() }}
            </span>
        </span>
    </a>
    @endif

    @if (Auth::user() && Auth::user()->is_admin &&  \App\revisorRequest::toBeCheckCount() > 0)
    <a class="dropdown-item" href="{{ route('admin.revisor.userRequestList') }}">
        <span class="h4 mr-2">
            <i class="fas fa-user-secret"></i> 
        </span>
        <span class="mr-2">
            Revisor Request
        </span>
        <span class="float-right">
            <span class="badge px-2 py-1 badge-danger">
                {{ \App\revisorRequest::toBeCheckCount() }}
            </span>
        </span>
    </a>
    @endif
    <hr class="dropdownusermenu">
    <a class="dropdown-item" href="{{ route('classifieds.index') }}">
        <span class="h4 mr-2">
            <i class="fas fa-ad"></i>
        </span>
        <span class="mr-2">
            Elenco annunci
        </span>
        <span class="floa-right">
            <span class="badge px-2 py-1 badge-info text-white mr-1">
                {{ \App\Classified::totalCount() }}
            </span>
            <span class="badge px-2 py-1 badge-success mx-1">
                {{ \App\Classified::totalAcceptedCount() }}
            </span>
            <span class="badge px-2 py-1 badge-danger mx-1">
                {{ \App\Classified::toBeRevisedCount() }}
            </span>
            <span class="badge px-2 py-1 badge-dark text-white ml-1">
                {{ \App\Classified::totalRefusedCount() }}
            </span>
    
        </span>

    </a>
 
    <a class="dropdown-item" href="{{ route('categories.index') }}">
        
        <span class="h4 mr-2">
            <i class="fas fa-stream"></i> 
        </span>
        <span class="mr-2">Elenco categorie</span>
        <span class="float-right">
         
            <span class="badge badge-info text-white px-2 py-1">
                {{ \App\Category::categoriesCount() }}
            </span>
        </span>
    
    
    </a>

    <a class="dropdown-item" href="{{ route('admin.users.list') }}">
       
            <span class="h4 mr-2">

                <i class="fas fa-users-cog"></i> 
            </span>
            <span class="mr-2">
    
                User List
            </span>

        <span class="float-right">
            <span class="badge px-2 py-1 badge-success mr-1">
                {{ \App\User::userCount() }}
            </span>
    
            <span class="badge px-2 py-1 badge-warning mx-1">
                {{ \App\revisorRequest::totalRevisor() }}
            </span>
            <span class="badge px-2 py-1 bg-secondary-zteam ml-1">
                {{ \App\User::adminCount() }}
            </span>
        </span>
    
    </a>
  
 <hr>
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
    <span class="h4 mr-2">
        <i class="fas fa-door-open"></i>
    </span>
    {{ __('Logout') }}</a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</form>
</div>
</div>
</li>