<div class="container my-3">
    <h2 class="text-center display-3"> Ultimi Articoli Inseriti </h2>
    <div class="row justify-content-center mt-5">
        <div class="col-12">
            
            <div id="lastArticle">
                
                
                @foreach (\App\Classified::last()->take(5) as $classified)
                @php
                $image = $classified->classifiedImages->pop();  
                
                if ($classified->is_accepted) {
                    $is_acceptedClass = "bg-success";
                } elseif ($classified->is_accepted != '0' && $classified->is_accepted != '1') {
                    
                    $is_acceptedClass = "bg-success";
                } else {
                    $is_acceptedClass = "bg-danger";
                }
                
                
                @endphp
                <div class=" px-4">
                    <div class="card">
                        <div class="card-body">
                            
                            <img src="{{$image->geturl(400,300)}}" class="card-img img-fluid" alt="{{ $classified->title }}">
                            
                            <h5 class="card-title mt-3">{{ $classified->title }}</h5>
                            <p class="card-text">Descrizione {{ $classified->description }}</p>
                            <p class="card-text">Località: {{ $classified->location }}</p>
                            <p class="card-text">Prezzo: {{ $classified->price }}</p>
                            <p class="card-text"> Venduto da: <a href="{{route('classifieds.categoryFiltered', $classified->category_id)}}">{{ $classified->category->title}}</a></p>
                            <p class="card-text"> Venduto da: <a href="{{route('user.article', $classified->user->id)}}">{{ $classified->user->name}}</a></p>
                            <a href="{{route('classifieds.show', compact('classified'))}}" class="btn btn-dark">Vai all'annuncio</a>
                        </div>
                    </div> 
                </div>
                @endforeach 
            </div>
        </div>
    </div>
</div>
@push('singlepagescript')
<script>
  
    $('#lastArticle').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        centerMode: true,
    });
</script>
@endpush