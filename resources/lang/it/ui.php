<?php

return [

        'welcome' => 'Benvenuto su',
        'category' => 'nella categoria',
        'place' => 'luogo',
        'article-desc' => 'descrizione articolo',
        'user' => 'utente',
        'title' => 'titolo',
        'description' => 'descrizione',
        'price' => 'prezzo',
        'place' => 'località',
        'images' => 'immagini',
        'image' => 'immagine',
        'adult' => 'adulti',
        'violence' => 'violenza',
        'racy' => 'razzismo',
        'spoof' => 'spoof',
        'medical' => 'medico',
];