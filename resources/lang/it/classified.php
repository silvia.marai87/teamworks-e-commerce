<?php

return [

        'name' => 'Nome',
        'description' => 'Descrizione',
        'images' => 'Immagini',
        'category' => 'Categoria',
        'price' => 'Prezzo',
        'location' => 'Località',
];