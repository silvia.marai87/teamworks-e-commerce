<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClassifiedController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/Articolo/{classified}/images', 'FrontController@classifiedImagesList')->name('single.classified.images');
Route::get('/', 'FrontController@index')->name('homepage');
Route::get('/diventa-revisore', 'FrontController@revisorLanding')->name('revisor.landing');



Route::get('/create/imgage/{classified}', 'FrontController@createNewImage')->name('classified.new.image')->middleware('auth');
Route::post('/update/profile/{classified}', 'FrontController@storeNewImgage')->name('classified.image.store')->middleware('auth');




// EDITING E REMOVE IMAGE ANNOUNCEMENT 



Route::delete('/adsImages/eliminare/{image}','FrontController@imageAdsDelete')->name('classified.images.delete')->middleware('auth');
// END IMAGE ANNOUNCEMENT

// ROTTE ADMIN
    // UTENTI
    Route::get('/admin/userlist', 'AdminController@userList')->name('admin.users.list')->middleware('auth.admin');
    Route::get('/admin/richiesteRevisori', 'AdminController@userRequestList')->name('admin.revisor.userRequestList')->middleware('auth.admin');
    Route::get('/admin/profile/user/{user}', 'AdminController@userprofile')->name('admin.user.profile')->middleware('auth');

    Route::get('/edit/profile/{user}', 'AdminController@usereditprofile')->name('admin.user.edit.profile')->middleware('auth.admin');
    Route::post('/update/profile/{user}', 'AdminController@userStoreProfile')->name('admin.user.update.profile')->middleware('auth.admin');

    Route::get('/admin/mur/{user}', 'AdminController@makeUserRevisor')->name('admin.makeUserRevisor')->middleware('auth.admin');
    Route::get('/admin/rur/{user}', 'AdminController@refuseUserRevisor')->name('admin.refuseUserRevisor')->middleware('auth.admin');

    // CRUD CATEGORY
    Route::get('/categorie','CategoryController@index')->name('categories.index')->middleware('auth.admin');
    Route::get('/categorie/nuova','CategoryController@create')->name('categories.create')->middleware('auth.admin');
    Route::post('/categorie/salvataggio','CategoryController@store')->name('categories.store')->middleware('auth.admin');
    Route::get('/categorie/modifica/{category}','CategoryController@edit')->name('categories.edit')->middleware('auth.admin');
    Route::put('/categorie/aggiorna/{category}','CategoryController@update')->name('categories.update')->middleware('auth.admin');
    Route::delete('/categorie/eliminare/{category}','CategoryController@delete')->name('categories.delete')->middleware('auth.admin');

    // CRUD CLASSIFIED
    Route::get('/annunci','ClassifiedController@index')->name('classifieds.index')->middleware('auth.admin');
    Route::delete('/annunci/eliminare/{classified}','ClassifiedController@delete')->name('classifieds.delete')->middleware('auth.admin');


// ROTTE REVISORI

    Route::get('/revisore', 'RevisorController@home')->name('revisor.home');
    Route::post('/revisore/annuncio/{id}/accept', 'RevisorController@accept')->name('revisor.accept');
    Route::post('/revisore/annuncio/{id}/reject', 'RevisorController@reject')->name('revisor.reject');
    Route::get('/revisore/richiesta/{user}', 'FrontController@revisorRequest')->name('revisor.request');

// ROTTE UTENTE
Route::get('/profile/', 'FrontController@userprofile')->name('user.profile')->middleware('auth');
Route::get('articoli/utente/{user}','FrontController@userArticle')->name('user.article');
Route::get('/edit/profile/', 'FrontController@usereditprofile')->name('user.edit.profile')->middleware('auth');
Route::post('/update/profile/', 'FrontController@userStoreProfile')->name('user.update.profile')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// ROTTE ANNUNCI UTENTE
Route::get('/annuncio/{classified}','ClassifiedController@show')->name('classifieds.show');
Route::get('/annunci/nuovo','ClassifiedController@create')->name('classifieds.create')->middleware('auth');
Route::post('/annunci/salvataggio','ClassifiedController@store')->name('classifieds.store');
Route::get('/annunci/modifica/{classified}','ClassifiedController@edit')->name('classifieds.edit')->middleware('auth');
Route::put('/annunci/aggiorna/{classified}','ClassifiedController@update')->name('classifieds.update');
Route::post('/annunci/images/upload', 'ClassifiedController@imageUpload')->name('classified.images.upload');
Route::delete('/annunci/images/remove', 'ClassifiedController@imageRemove')->name('classified.images.remove');
Route::get('/annunci/images','ClassifiedController@getImages')->name('classified.images');

// FILTRO CATEGORIA
Route::post('/classified/filter/', 'FrontController@categoryFilter')->name('classifieds.categoryFilter');
Route::get('/classified/category/{category}', 'FrontController@categoryFiltered')->name('classifieds.categoryFiltered');

// SEARCH
Route::get('/ricerca', 'FrontController@search')->name('search');

// LOCALIZATION
Route::post('/locale/{locale}', 'FrontController@locale')->name('locale');