<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Classified extends Model
{
    use Searchable;

    public function toSearchableArray()
    {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
            'location' => $this->location,
            'altro' => 'annunci Giovanni'
        ];
        
        
        return $array;
    }

    protected $fillable = [
        'title',
        'description',
        'category_id',
        'price',
        'location',
        'status',
        'user_id'
    ];

    public function category() 
    {
        return $this->belongsTo('App\Category');
    }
    public function user() 
    {
        return $this->belongsTo('App\User');
    }

    static public function lastFive ()
    {
        $lastfive = Classified::orderBy('id')->take(5)->get();
        return $lastfive;
    }
    static public function last ()
    {
        $lastfive = Classified::orderBy('id')->where('is_accepted', true)->get();
        return $lastfive;
    }
    static public function lastTen ()
    {
        $lastTen = Classified::orderBy('id')->where('is_accepted', true)->take(10)->get();
        return $lastTen;
    }
    static public function categoryFilter ($category)
    {
        $classifieds = $category->classifieds()->get();
        return $classifieds;
    }
    
    

    static public function toBeRevisedCount()
    {
        return Classified::where('is_accepted', null)->count();
    }
    static public function totalAcceptedCount()
    {
        return Classified::where('is_accepted', true)->count();
    }
    static public function totalRefusedCount()
    {
        return Classified::where('is_accepted', '=', '0')->count();
    }
    static public function totalRefused()
    {
        $totalRefused = Classified::where('is_accepted', '=', '0')->get();
        return $totalRefused;
    }
    static public function totalCount()
    {
        return Classified::all()->count();
    }
    
    // SPECIFIC USER COLLECTOR
    
    static public function userRefusedAds($user)
    {
        $userRefuseAds = $user->userClassifieds->where('is_accepted', '=' ,'0');
        return $userRefuseAds;
    }

    static public function userRefusedAdsCount($user)
    {
        $userRefuseAdsCount = $user->userClassifieds->where('is_accepted', '=' ,'0')->count();
        return $userRefuseAdsCount;
    }
    static public function userPendingdAds($user)
    {
        $userPendingAds = $user->userClassifieds->where('is_accepted', '!=' ,'0')->where('is_accepted', '!=' ,'1');
        return $userPendingAds;
    }

    static public function userPendingAdsCount($user)
    {
        $userPendingAdsCount = $user->userClassifieds->where('is_accepted', '!=' ,'0')->where('is_accepted', '!=' ,'1')->count();
        return $userPendingAdsCount;
    }
    static public function userAppovedAds($user)
    {
        $userApprovedAds = $user->userClassifieds->where('is_accepted', true);
        return $userApprovedAds;
    }

    static public function userAppovedAdsCount($user)
    {
        $userApprovedAdsCount = $user->userClassifieds->where('is_accepted', true)->count();
        return $userApprovedAdsCount;
    }


    //
    static public function categoryAds($category) 
    {
        $filterdAds = Classified::where('category_id',$category->id)->where('is_accepted', true)->get();
        return $filterdAds;

    }
    static public function categoryAdsCount($category) 
    {
           
    
        $filterdAds = Classified::where('category_id',$category->id)->where('is_accepted', true)->count();
        return $filterdAds;
     

    }


    static public function totalUserImageLoad($user)
    {
        $totalImage = 0;
        $totalUserAds = $user->userClassifieds;
        foreach ($totalUserAds as $ads){

            $thisAdsImageCount = $ads->classifiedImages->count();
            $totalImage = $thisAdsImageCount + $totalImage;
        }
        return $totalImage;
    }
    


    public function classifiedImages ()
    {
        return $this->hasMany('App\ClassifiedImage');
    }
}
