<?php

namespace App;

use App\Classified;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ClassifiedImage extends Model
{
    protected $casts = [
        'labels' => 'array',
        
    ];
    protected $fillable = [
        'classified_id',
        'file'
    ];
    
    public function classified ()
    {
        return $this->belongsTo('App\Classified');
    }
    
    
    static public function getUrlByFilePath($filePath,  $w = null, $h = null)
    {
        if (!$w && !$h) {
            return Storage::url($filePath);
        }
        
        $path = dirname($filePath);
        $filename = basename($filePath);
        
        $file = "{$path}/crop{$w}x{$h}_{$filename}";
        return Storage::url($file);
    }
    
    public function getUrl($w = null, $h = null)
    {
        return ClassifiedImage::getUrlByFilePath($this->file, $w, $h);
    }

    static public function totalCount()
    {
        return ClassifiedImage::all()->count();
    }
    
}
