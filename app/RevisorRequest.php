<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisorRequest extends Model
{
    protected $fillable = [
        'user_id',
        'user_email',
        'status'
    ];
    static public function toBeCheckCount()
    {
        return RevisorRequest::all()->count();
    }

    static public function totalRevisor()
    {
        return User::where('is_revisor', true)->count();
    }
}
