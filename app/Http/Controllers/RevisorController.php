<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Classified;
use Illuminate\Support\Facades\Auth;

class RevisorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.revisor');
    }
    
    public function home()
    {
        $classified = Classified::where('is_accepted', null)
            ->orderBy('created_at', 'desc')
            ->first();
        return view('revisor.home', compact('classified'));
    }

    private function setAccepted($classified_id, $value)
    {
        $user = Auth::user();
        if ($user && $user->is_revisor) {
        $classified = Classified::find($classified_id);
        $classified->is_accepted = $value;
        $classified->save();
        };
        return redirect(route('revisor.home'));
       

    }

    public function accept($classified_id)
    {
       


        return $this->setAccepted($classified_id, true);
    }

    public function reject($classified_id)
    {
        return $this->setAccepted($classified_id, false);
    }
}