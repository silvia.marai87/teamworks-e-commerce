<?php

namespace App\Http\Controllers;


use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        // dd($request->file('categoryThumb'));
        Category::create(
        [ 'title'=>$request->input('title'),
          'shortTitle'=>$request->input('shortTitle'),
          'categoryThumb'=>$request->file('categoryThumb')->store('public/category/thumb'),
          'categoryHeader'=>$request->file('categoryHeader')->store('public/category/header'),
        ]
        );
        $categories = Category::all();
        View::share('categories', $categories);
        
        return redirect()->route('categories.index')->with('message', 'categoria inserita correttamente');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {   
        // dd($request->file('categoryThumb'));
        $catThumbCheck = empty($request->file('categoryThumb'));
        $catHeaderCheck = empty($request->file('categoryHeader'));
        
        // if (isset($request->input('categoryThumb')) && isset($request->input('categoryHeader'))) {
        //     return;
        // }
        // dd($catThumbCheck);
        if (!$catThumbCheck && !$catHeaderCheck)
            {                
               
                $category->update([
 
                    'title'=>$request->input('title'),
                    'shortTitle'=>$request->input('shortTitle'),
                    'categoryThumb'=>$request->file('categoryThumb')->store('public/category/thumb'),
                    'categoryHeader'=>$request->file('categoryHeader')->store('public/category/header'),
                    ]);
            } elseif (!$catThumbCheck && $catHeaderCheck) {
               
           
               
                $category->update([
                    
                  
                    'title'=>$request->input('title'),
                    'shortTitle'=>$request->input('shortTitle'),
                    'categoryThumb'=>$request->file('categoryThumb')->store('public/category/thumb'),
                ]);
            } elseif ($catThumbCheck && !$catHeaderCheck) {


               
                $category->update([

                    'title'=>$request->input('title'),
                    'shortTitle'=>$request->input('shortTitle'),
                    'categoryHeader'=>$request->file('categoryHeader')->store('public/category/header'),
                ]);

            } else {

               
                $category->update([

                    'title'=>$request->input('title'),
                    'shortTitle'=>$request->input('shortTitle'),
                ]);
            }



      

        return redirect()->route('categories.index')->with('message','Complimenti, categoria modificata con successo!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete(Category $category)
    {
        $classifieds = $category->classifieds;

        foreach ($classifieds as $classified) {
            $classifiedImages = $classified->classifiedImages;
            foreach ($classifiedImages as $image){
            $image->delete();
         }
            $classified->delete();
        }



        $category->delete();
        return redirect()->route('categories.index')->with('message','Complimenti, categoria eliminata con successo!');
    }
}
