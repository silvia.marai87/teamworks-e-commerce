<?php

namespace App\Http\Controllers;

use App\RevisorRequest;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
  public function makeUserRevisor($user){
    $user = User::find($user);
    $userEmail = $user->email;
   
      $request = RevisorRequest::where('user_email', $userEmail)->get();
      $check = json_decode(json_encode($request), true);
     
      if (count($check) > 0) {
        
      
      $user->is_revisor = 1;
      RevisorRequest::where('user_id',$user->id)->delete();
      $user->save();
      return redirect()->back()->with('message', "L'utente $user->name è ora Revisore");
      } else {
        return redirect()->back()->with('message', "L'utente $user->name non ha richiesto di essere Revisore");

      }

  }
  public function refuseUserRevisor($user){
    
    $request = RevisorRequest::where('user_id', $user)->get();
    $user = User::find($user);
    $user->is_revisor = false;
    $request->delete();
    $user->save();
    return redirect()->back()->with('message', "L'utente $user->name è ora Revisore");

  }

  public function userList()
  {
      $users = User::all();
      return view('admin.users.listall', compact('users'));
  }

  public function userRequestList()
  {
    $requestList = RevisorRequest::all();
    $newUsers = [0 => ''];
  
    if(count($requestList) > 1) {

      foreach($requestList as $request) {

        $users = User::where('email', $request->user_email)->get();
        $users = json_decode(json_encode($users), true);

        
        $newUsers = array_filter(array_merge($newUsers, $users));
            
      }
         
  
    }
     elseif (count($requestList) > 0) {
      $requestList = json_decode(json_encode($requestList), true);
      $requestList = $requestList[0];
  
      $users = User::where('email', $requestList['user_email'])->get();
      $users = json_decode(json_encode($users), true);

      
      $newUsers = array_filter(array_merge($newUsers, $users));
    }
    else {

    }
 


    return view('admin.users.revisorRequests', compact('newUsers','requestList'));
  }

  public function userProfile($user) {
    
    $user = User::find($user);

    return view ('users.profile', compact('user'));
  }
  public function usereditprofile($user)
    {
        $user = User::find($user);
        return view ('users.editprofile', compact('user'));
    }
    public function userStoreProfile(Request $request)
    {
        // dd($request->all());
        $user = User::find($request->user);
        // dd($user);
        if (isset($request->avatar)){


            $user->update([
                'name'=>$request->input('title'),
                'avatar'=>$request->file('avatar')->store('public/user/avatar/'),
                ]);
            } else {
                $user->update([
                    'name'=>$request->input('title'),
                    ]);

            }
        return redirect()->back()->with('message','profilo editato');
  
}
}
