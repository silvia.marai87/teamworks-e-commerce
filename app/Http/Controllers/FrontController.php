<?php

namespace App\Http\Controllers;

use App\Category;
use App\Classified;
use App\ClassifiedImage;
use App\RevisorRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $user_id = Auth::id();
        
        $user = Auth::user();
        $classifiedscarosel = Classified::orderBy('id')->take(5)->get();
        return view('welcome', compact('classifiedscarosel', 'user_id', 'user'));
    }
    
    public function categoryFilter(Request $request)
    {
        $category = $request->all();
        $category = $category['category_id'];
        return redirect()->route('classifieds.categoryFiltered', compact('category'));
        
    }
    
    public function categoryFiltered($category)
    {
        
        $category = \App\Category::find($category);
        $classifieds = $category->classifieds()->where('is_accepted', true)->get();
        
        
        return view('classifieds.filtered', compact('classifieds'), compact('category'));
    }
    
    public function search(Request $request){
        
        $q = $request->input('q');
        $category = $request->input('category_id');
        $classifieds = Classified::search($q)->where('is_accepted', true)->get();
        if ($category != ""){
            $classifieds = $classifieds->where('category_id',$category);
        }
        return view('searchResult', compact('classifieds', 'q'));
    } 



    public function userprofile()
    {
        $user = Auth::user();
        $annuncements = $user->annuncements;
        return view ('users.profile', compact('user', 'annuncements'));
    }
    public function usereditprofile()
    {
        $user = Auth::user();
        return view ('users.editprofile', compact('user'));
    }



    public function userStoreProfile(Request $request)
    {
        // dd($request->all());
        $user = User::find($request->user);
        // dd($user);
        if (isset($request->avatar)){


            $user->update([
                'name'=>$request->input('title'),
                'avatar'=>$request->file('avatar')->store('public/user/avatar/'),
                ]);
            } else {
                $user->update([
                    'name'=>$request->input('title'),
                    ]);

            }
        return redirect()->route('user.profile');
    }
    
    
    public function locale($locale)
    {
        session()->put('locale', $locale);
        return redirect()->back();
    }

    public function userArticle($user)
    {
        $user = User::find($user);
       
        $classifieds = $user->userclassifieds;

        return view ('users.userArticle', compact('classifieds','user'));
    }

    public function revisorRequest($user)
    {
        $user = User::find($user);
        $usercheck = Auth::user();
        if ($user != $usercheck) {
            return redirect()->route('homepage')->with('message', 'AIUTO UN HACKER SCAPPATE');
        }
        if ($user->is_revisor){
            return redirect()->route('homepage')->with('message', 'Sei già revisore e che vuoi');
        }
        $revisorList = \App\RevisorRequest::where('user_email', $user->email)->get();
        $revisorList =   json_decode(json_encode($revisorList), true);
        
        if (count($revisorList) > 0) {
          return redirect()->route('homepage')->with('message', 'Hai Già Una Richiesta Attiva');
        } else {
        RevisorRequest::create(
            [ 
                'user_id'=>$user->id,
                'user_email'=>$user->email,
            ]);

        return redirect()->back()->with('message', 'Richiesta inviata correttamente');
            }
    }

    public function revisorLanding()
    {
        return view('revisor.landing');
    }

    public function classifiedImagesList($classified)
    {
        $classified = Classified::find($classified);
        
        $images = $classified->classifiedImages;
        // dd($images);
        return view('classifieds.imagesClassifiedlist',compact('classified','images'));
    }

    public function editClassifiedImage($image)
    {
        // $image = $image;        

        // return view('classified.images.edit', compact('image'));
    }
    public function storeClassifiedImage(Request $request, $image)
    {
        // dd($request->all());
        // $image = ClassifiedImage::find($image);
        // $authUser = Auth::user();

        // if ($image->user->id !== $authUser->id){

        //     return redirect()->back()->with('message', 'Qualcosa non quadra, non sei il proprietario');
        // } else {

        //     $image->update([

        //         'file' => $request->file('avatar')->store('public/user/avatar/')

        //     ]);
        //         return redirect()->back()->with('message', 'Immagine Editata Correttamente');
        // }
        return;
    }
    
    public function imageAdsDelete($image) 
    {
        $image = ClassifiedImage::find($image);
        if (Auth::user() != $image->user->id){
            return redirect()->route('home')->with('message','è tutto protetto');
        } else{

            $image->delete();
            return redirect()->back()->with('message','immagine rimossa');
        }

    }

    public function createNewImage($classified){

        return view('classifieds.images.create', compact('classified'));
    }

    public function storeNewImgage(Request $request, $classified){

        
        $classified = Classified::find($classified);
        if (Auth::user()->id != $classified->user->id){
            return redirect()->route('home')->with('message','è tutto protetto');
        } else{

            ClassifiedImage::create([

                'classified_id'=>$classified->id,
                'file'=>$request->file('file')->store('public/classifieds/images'),
                'labels'=>'giovanni',
                // adult
                // spoof 
                // violence
                // medical
                // racy
                
                ]);
            return redirect()->back()->with('message','Immagine Aggiunta');
        }

    }
}
