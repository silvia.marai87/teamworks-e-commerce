<?php

namespace App\Jobs;

use App\ClassifiedImage;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;


class ZTeamWaterMark implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $classified_image_id;
    
    public function __construct($classified_image_id)
    {
        $this->classified_image_id = $classified_image_id;
    }

    public function handle()
    {
        $i = ClassifiedImage::find($this->classified_image_id);
        if(!$i) {
            return;
        }

        $srcPath = storage_path('/app/' . $i->file);
        $image = file_get_contents($srcPath);

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('googlevision.json'));


       

            $image = Image::load($srcPath);

            $image->watermark(base_path("public/images/logo.png"))
                    ->watermarkPosition(Manipulations::POSITION_BOTTOM_RIGHT)
                    ->watermarkPadding(15, 10, Manipulations::UNIT_PERCENT)
                    ->watermarkOpacity(50)
                    ->watermarkHeight(20, Manipulations::UNIT_PERCENT)    // 20 percent height
                    ->watermarkWidth(20, Manipulations::UNIT_PERCENT)   // 20 percent width
                    ->watermarkFit(Manipulations::FIT_STRETCH);

            $image->save($srcPath);




    }
}
