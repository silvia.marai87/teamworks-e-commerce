<?php

namespace App\Jobs;

use App\ClassifiedImage;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GoogleVisionSafeSearchImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    private $classified_image_id;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($classified_image_id)
    {
        $this->classified_image_id = $classified_image_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $i = ClassifiedImage::find($this->classified_image_id);
        if (!$i) { return; }

        $image = file_get_contents(storage_path('/app/' . $i->file));

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('googlevision.json'));
        
        $imageAnnotator = new ImageAnnotatorClient();
        $response = $imageAnnotator->safeSearchDetection($image);
        $imageAnnotator->close();

        $safe = $response->getSafeSearchAnnotation();

        $adult = $safe->getAdult();
        $medical = $safe->getMedical();
        $spoof = $safe->getSpoof();
        $violence = $safe->getViolence();
        $racy = $safe->getRacy();

       /*  echo json_encode([$adult, $medical, $spoof, $violence, $racy]); */

        $levelResponse = [ '0', '20', '40', '60', '80', '100' ];

        $i->adult = $levelResponse[$adult];
        $i->medical = $levelResponse[$medical];
        $i->spoof = $levelResponse[$spoof];
        $i->violence = $levelResponse[$violence];
        $i->racy = $levelResponse[$racy];
        
        $i->save();

    }
}
