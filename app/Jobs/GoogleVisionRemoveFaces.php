<?php

namespace App\Jobs;

use App\ClassifiedImage;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;


class GoogleVisionRemoveFaces implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $classified_image_id;
    
    public function __construct($classified_image_id)
    {
        $this->classified_image_id = $classified_image_id;
    }

    public function handle()
    {
        $i = ClassifiedImage::find($this->classified_image_id);
        if(!$i) {
            return;
        }

        $srcPath = storage_path('/app/' . $i->file);
        $image = file_get_contents($srcPath);

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('googlevision.json'));

        $imageAnnotator = new ImageAnnotatorClient();
        $response = $imageAnnotator->faceDetection($image);
        $faces = $response->getFaceAnnotations();

        foreach ($faces as $face) {

            $vertices = $face->getBoundingPoly()->getVertices();

            $bounds = [];
            foreach ($vertices as $vertex) {

                $bounds[] = [$vertex->getX(), $vertex->getY()];

            }
            $watermark = mt_rand(1,5);

            $w = $bounds[2][0] - $bounds[0][0];
            $h = $bounds[2][1] - $bounds[0][1];

            $image = Image::load($srcPath);

            $image->watermark(base_path("public/images/watermark_face/$watermark.png"))
                    ->watermarkPosition('top-left')
                    ->watermarkPadding($bounds[0][0], $bounds[0][1])
                    ->watermarkWidth($w, Manipulations::UNIT_PIXELS)
                    ->watermarkHeight($h, Manipulations::UNIT_PIXELS)
                    ->watermarkFit(Manipulations::FIT_STRETCH);

            $image->save($srcPath);


        }

        $imageAnnotator->close();

    }
}
