<?php

namespace App\Jobs;

use App\ClassifiedImage;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GoogleVisionLabelImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $classified_image_id;

    public function __construct($classified_image_id)
    {
        $this->classified_image_id = $classified_image_id;
    }

    public function handle()
    {
        $i = ClassifiedImage::find($this->classified_image_id);
        if (!$i) { return; }

        $image = file_get_contents(storage_path('/app/' . $i->file));

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('googlevision.json'));

        $imageAnnotator = new ImageAnnotatorClient();
        $response = $imageAnnotator->labelDetection($image);
        $labels = $response->getLabelAnnotations();

        if ($labels) {

            $result = [];
            foreach ($labels as $label) {
                $result[] = $label->getDescription();
            }

            // echo json_encode($result);
            $i->labels = $result;
            $i->save();
        }

        $imageAnnotator->close();
    }
}
