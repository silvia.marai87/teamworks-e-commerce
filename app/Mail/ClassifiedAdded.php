<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClassifiedAdded extends Mailable
{
    use Queueable, SerializesModels;

    public $classified;

    public function __construct($classified)
    {
        $this->classified = $classified;
    }

    public function build()
    {
        return $this->subject('Annuncio inserito con successo!')->view('emails.classified.created');
    }
}
