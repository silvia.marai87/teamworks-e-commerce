<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     * - Nome
    *- Cognome
    *- Indirizzo
    *- Città
    *- Provincia
    *- CAP
    *- Nazione
    *- Email
    *- Codoce Fiscale
    *- Sesso
    *- Data di nascita
    *- Consenso privacy 1 (base)
    *- Consenso privacy 2 (marketing)
    *- Consenso privacy 3 (profilazione)
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
